s3-upload-maven-plugin
======================
Uploads a file or (recursively) the contents of a directory to S3.

Configuration parameters
------------------------

| Parameter       | Description                                                                               | Required | Default                                                                  |
|-----------------|-------------------------------------------------------------------------------------------|----------|--------------------------------------------------------------------------|
| bucketName      | The name of the bucket                                                                    | *yes*    |                                                                          |
| source          | The source file or folder                                                                 | *yes*    |                                                                          |
| destination     | The destination file or destination folder (omit to upload to the bucket root)            | *no*     |                                                                          |
| recursive       | If this is a directory copy, recursively copy all contents                                | *no*     | false                                                                    |
| accessKey       | S3 access key                                                                             | *no*     | if unspecified, uses the Default Provider, falling back to env variables |
| secretKey       | S3 secret key                                                                             | *no*     | if unspecified, uses the Default Provider, falling back to env variables |
| doNotUpload     | Dry run                                                                                   | *no*     | false                                                                    |
| endpoint        | Use a different s3 endpoint                                                               | *no*     | s3.amazonaws.com                                                         |
| region          | Use a different s3 region                                                                 | *no*     | us-west-2                                                                |
| cannedACL       | ACL of the uploaded objects, one of Private, PublicRead, PublicReadWrite,                 | *no*     | BucketOwnerFullControl                                                   |
|                 | AuthenticatedRead, LogDeliveryWrite, BucketOwnerRead, BucketOwnerFullControl, AwsExecRead |          |                                                                          |
| pathAddressing  | Whether to force path addressing (true) or use path addressing only when needed (false)   | *no*     | false                                                                    |
      

Example: Upload a file
----------------------
```xml
<build>
  ...

  <plugins>
    ...

    <plugin>
      <groupId>com.bazaarvoice.maven.plugins</groupId>
      <artifactId>s3-upload-maven-plugin</artifactId>
      <version>1.4</version>
      <configuration>
        <bucketName>my-s3-bucket</bucketName>
        <source>dir/filename.txt</source>
        <destination>remote-dir/remote-filename.txt</destination>
      </configuration>
    </plugin>
  </plugins>
</build>
```

Example: Recursively upload a folder
------------------------------------
```xml
<build>
  ...

  <plugins>
    ...

    <plugin>
      <groupId>com.bazaarvoice.maven.plugins</groupId>
      <artifactId>s3-upload-maven-plugin</artifactId>
      <version>1.4</version>
      <configuration>
        <bucketName>my-s3-bucket</bucketName>
        <source>dir</source>
        <destination>remote-dir</destination>
        <recursive>true</recursive>
      </configuration>
    </plugin>
  </plugins>
</build>
```

