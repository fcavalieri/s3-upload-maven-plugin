package com.bazaarvoice.maven.plugins.s3.download;

import com.amazonaws.auth.*;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.ObjectMetadataProvider;
import com.amazonaws.services.s3.transfer.Transfer;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;

@Mojo(name = "s3-download")
public class S3DownloadMojo extends AbstractMojo
{
  /** Access key for S3. */
  @Parameter(property = "s3-download.accessKey")
  private String accessKey;

  /** Secret key for S3. */
  @Parameter(property = "s3-download.secretKey")
  private String secretKey;

  /** The file/folder (in the bucket) to download. */
  @Parameter(property = "s3-download.source", defaultValue = "")
  private String source;

  /** The bucket to upload into. */
  @Parameter(property = "s3-download.bucketName", required = true)
  private String bucketName;

  /** The local folder to download to. */
  @Parameter(property = "s3-download.destination", required = true)
  private File destination;

  /** Override S3 endpoint. */
  @Parameter(property = "s3-download.endpoint")
  private String endpoint;

  /** Overrides default S3 region. */
  @Parameter(property = "s3-download.region", defaultValue = "us-west-2")
  private String region;

  /** Whether to use path addressing or not. */
  @Parameter(property = "s3-download.pathAddressing", defaultValue = "false")
  private boolean pathAddressing;

  /** Whether to use path addressing or not. */
  @Parameter(property = "s3-download.ignoreMissing", defaultValue = "false")
  private boolean ignoreMissing;

  @Override
  public void execute() throws MojoExecutionException
  {
    AmazonS3 s3 = getS3Client(accessKey, secretKey, endpoint, region, pathAddressing);

    if (!s3.doesBucketExistV2(bucketName)) {
      throw new MojoExecutionException("Bucket doesn't exist: " + bucketName);
    }

    boolean success = download(s3, source);
    if (!success) {
      throw new MojoExecutionException("Unable to download file/folder from S3.");
    }

    getLog().info(String.format("File %s uploaded to s3://%s/%s",
            source, bucketName, destination));
  }

  private static AmazonS3 getS3Client(String accessKey, String secretKey, String endpoint, String region, boolean pathAddressing)
  {
    AWSCredentialsProvider provider;
    if (accessKey != null && secretKey != null) {
      AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
      provider = new AWSStaticCredentialsProvider(credentials);
    } else {
      provider = new DefaultAWSCredentialsProviderChain();
    }

    AmazonS3ClientBuilder builder = AmazonS3Client.builder()
            .withCredentials(provider);
    if (endpoint != null) {
      builder.setEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region));
    } else {
      builder.setRegion(region);
    }
    if (pathAddressing) {
      builder.setPathStyleAccessEnabled(true);
    }
    return builder.build();
  }

  private boolean download(AmazonS3 s3, String source) throws MojoExecutionException
  {
    TransferManager mgr = TransferManagerBuilder.standard()
            .withS3Client(s3)
            .build();

    Transfer transfer;
    if (source == null || source.isBlank()) {
      transfer = mgr.downloadDirectory(bucketName, source, destination);
    }
    else if (source.endsWith("/")) {
      if (s3.listObjectsV2(bucketName, source).getKeyCount() == 0) {
        if (ignoreMissing) {
          getLog().warn(String.format("Folder %s does not exist in bucket %s", source, bucketName));
        } else {
          throw new MojoExecutionException(String.format("Folder %s does not exist in bucket %s", source, bucketName));
        }
      }
      transfer = mgr.downloadDirectory(bucketName, source, destination);
    } else {
      if (!s3.doesObjectExist(bucketName, source)) {
        if (ignoreMissing) {
          getLog().warn(String.format("Key %s does not exist in bucket %s", source, bucketName));
        } else {
          throw new MojoExecutionException(String.format("Key %s does not exist in bucket %s", source, bucketName));
        }
      }
      transfer = mgr.download(bucketName, source, destination);
    }
    try {
      getLog().debug("Transferring " + transfer.getProgress().getTotalBytesToTransfer() + " bytes...");
      transfer.waitForCompletion();
      getLog().info("Transferred " + transfer.getProgress().getBytesTransferred() + " bytes.");
    } catch (InterruptedException e) {
      return false;
    }

    return transfer.getState() == Transfer.TransferState.Completed;
  }
}
