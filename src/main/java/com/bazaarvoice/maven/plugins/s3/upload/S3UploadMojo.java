package com.bazaarvoice.maven.plugins.s3.upload;

import com.amazonaws.auth.*;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.ObjectMetadataProvider;
import com.amazonaws.services.s3.transfer.Transfer;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;

@Mojo(name = "s3-upload")
public class S3UploadMojo extends AbstractMojo
{
  /** Access key for S3. */
  @Parameter(property = "s3-upload.accessKey")
  private String accessKey;

  /** Secret key for S3. */
  @Parameter(property = "s3-upload.secretKey")
  private String secretKey;

  /**
   *  Execute all steps up except the upload to the S3.
   *  This can be set to true to perform a "dryRun" execution.
   */
  @Parameter(property = "s3-upload.doNotUpload", defaultValue = "false")
  private boolean doNotUpload;

  /** The file/folder to upload. */
  @Parameter(property = "s3-upload.source", required = true)
  private File source;

  /** The bucket to upload into. */
  @Parameter(property = "s3-upload.bucketName", required = true)
  private String bucketName;

  /** The file/folder (in the bucket) to create. */
  @Parameter(property = "s3-upload.destination", defaultValue = "")
  private String destination;

  /** Override S3 endpoint. */
  @Parameter(property = "s3-upload.endpoint")
  private String endpoint;

  /** Overrides default S3 region. */
  @Parameter(property = "s3-upload.region", defaultValue = "us-west-2")
  private String region;

  /** In the case of a directory upload, recursively upload the contents. */
  @Parameter(property = "s3-upload.recursive", defaultValue = "false")
  private boolean recursive;

  @Parameter(property = "s3-upload.cannedACL", defaultValue = "BucketOwnerFullControl")
  private CannedAccessControlList cannedACL;

  /** Whether to use path addressing or not. */
  @Parameter(property = "s3-upload.pathAddressing", defaultValue = "false")
  private boolean pathAddressing;

  @Override
  public void execute() throws MojoExecutionException
  {
    if (!source.exists()) {
      throw new MojoExecutionException("File/folder doesn't exist: " + source);
    }

    AmazonS3 s3 = getS3Client(accessKey, secretKey, endpoint, region, pathAddressing);

    if (!s3.doesBucketExistV2(bucketName)) {
      throw new MojoExecutionException("Bucket doesn't exist: " + bucketName);
    }

    if (doNotUpload) {
      getLog().info(String.format("File %s would have be uploaded to s3://%s/%s (dry run)",
              source, bucketName, destination));

      return;
    }

    boolean success = upload(s3, source);
    if (!success) {
      throw new MojoExecutionException("Unable to upload file to S3.");
    }

    getLog().info(String.format("File %s uploaded to s3://%s/%s",
            source, bucketName, destination));
  }

  private static AmazonS3 getS3Client(String accessKey, String secretKey, String endpoint, String region, boolean pathAddressing)
  {
    AWSCredentialsProvider provider;
    if (accessKey != null && secretKey != null) {
      AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
      provider = new AWSStaticCredentialsProvider(credentials);
    } else {
      provider = new DefaultAWSCredentialsProviderChain();
    }

    AmazonS3ClientBuilder builder = AmazonS3Client.builder()
            .withCredentials(provider);
    if (endpoint != null) {
      builder.setEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region));
    } else {
      builder.setRegion(region);
    }
    if (pathAddressing) {
      builder.setPathStyleAccessEnabled(true);
    }
    return builder.build();
  }

  private boolean upload(AmazonS3 s3, File sourceFile) throws MojoExecutionException
  {
    TransferManager mgr = TransferManagerBuilder.standard()
            .withS3Client(s3)
            .build();

    Transfer transfer;
    if (sourceFile.isFile()) {
      transfer = mgr.upload(new PutObjectRequest(bucketName, destination, sourceFile)
              .withCannedAcl(cannedACL));
    } else if (sourceFile.isDirectory()) {
      transfer = mgr.uploadDirectory(bucketName, destination, sourceFile, recursive,
              new ObjectMetadataProvider() {
                @Override
                public void provideObjectMetadata(final File file, final ObjectMetadata objectMetadata) {
                  /**
                   * This is a terrible hack, but the SDK as of 1.10.69 does not allow setting ACLs
                   * for directory uploads otherwise.
                   */
                  objectMetadata.setHeader(Headers.S3_CANNED_ACL, CannedAccessControlList.BucketOwnerFullControl);
                }
              });
    } else {
      throw new MojoExecutionException("File is neither a regular file nor a directory " + sourceFile);
    }
    try {
      getLog().debug("Transferring " + transfer.getProgress().getTotalBytesToTransfer() + " bytes...");
      transfer.waitForCompletion();
      getLog().info("Transferred " + transfer.getProgress().getBytesTransferred() + " bytes.");
    } catch (InterruptedException e) {
      return false;
    }

    return transfer.getState() == Transfer.TransferState.Completed;
  }
}
